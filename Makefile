all: remove_containers start

remove_containers:
	docker compose rm -f service
	docker compose rm -f database

start:
	docker compose build
	docker compose up
