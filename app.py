import datetime
import json

from flask import Flask, request, jsonify
from flask_mongoengine import MongoEngine
import sympy
import random


app = Flask(__name__)

app.config['MONGODB_SETTINGS'] = {
    'db': 'key_service',
    'host': 'elgamal-key-service-database-1',
    'port': 27017
}
db = MongoEngine()
db.init_app(app)


class Key(db.Document):
    key_id = db.StringField()
    p = db.StringField()
    g = db.StringField()
    y = db.StringField()
    x = db.StringField()

    def __str__(self):
        return {'key_id': self.key_id, 'p': self.p, 'g': self.g, 'y': self.y, 'x': self.x}.__str__()


@app.route("/key_gen", methods=['POST'])
def key_gen():
    req = request.get_json()

    print(req)

    p = sympy.randprime(2 ** 63, 2 ** 64)
    g = sympy.primitive_root(p)
    x = random.randint(2 ** 62, 2 ** 63)
    y = pow(g, x, p)

    Key.save(Key(key_id=req['key_id'], p=str(p), g=str(g), y=str(y), x=str(x)))

    return {"p": str(p), "g":  str(g), "y": str(y)}, 200


@app.route('/decrypt', methods=['GET'])
def decrypt():
    key_id = request.args.get('key_id')
    key = Key.objects(key_id=key_id)[0]

    x = int(key['x'])
    p = int(key['p'])

    a = int(request.args.get('a'))
    b = int(request.args.get('b'))

    return jsonify(aggregatedResult=str(pow(pow(a, x, p), -1, p) * b % p)), 200


@app.route('/test', methods=['GET'])
def test():
    return "OK", 200
