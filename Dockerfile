FROM python:3.8-alpine

EXPOSE 5000

RUN mkdir /app/
WORKDIR /app/

COPY ./app.py .
COPY ./requirements.txt .

RUN python3 -m venv venv/
ENV PATH="venv/bin:$PATH"
RUN pip install -r requirements.txt

ENTRYPOINT ["flask", "run", "--host=0.0.0.0"]
